
# Skill calls will begin at the evaluate() method, with the skill payload passed as 'payload'
# and the skill context passed as 'context'.
from dataclasses import asdict

from src.model import Request, Response


def evaluate(payload: dict, context: dict) -> dict:
    print(payload)
    print(context)

    request: Request = Request(payload["request"])
    response: Response = Response(request.request)

    return asdict(response)


def on_started(context: dict):
    print("on_started triggered!")


def on_stopped(context: dict):
    print("on_stopped triggered!")
